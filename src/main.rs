#[macro_use] extern crate rocket;

mod rust_atp;

use crate::rust_atp::{
    db::OperationsStore,
    did::{
        operations::{
            ValidatedSignedOperation,
        },
        DidDocData,
    }
};
use mongodb::bson::doc;
use rocket::State;
use serde_json::json;

#[rocket::main]
async fn main() {
    rocket::build()
        .manage(OperationsStore::new().await)
        .mount("/", routes![
            index,
            health,
            get_did,
            get_did_data,
            get_did_log,
            post_did_operation,
        ])
        .launch()
        .await;
}

#[get("/")]
fn index() -> &'static str {
    "This is a DID:PLC server. See https://atproto.com/specs/did-plc"
}

#[get("/_health")]
fn health() -> &'static str {
    "OK"
}

#[get("/<did>")]
fn get_did(did: String) -> String {
    format!("This will send the document associated with {}, encoded as a JSON string.", did)
}

#[get("/data/<did>")]
fn get_did_data(did: String) -> String {
    format!("This will send the document data associated with {}, encoded as a JSON string.", did)
}

#[get("/log/<did>")]
async fn get_did_log(ops_store: &State<OperationsStore>, did: String) -> serde_json::Value {
    //format!("This will send any did operations associated with {}, encoded as a JSON string.", did)
    match find_did_operations(ops_store, did).await {
        Some(did_doc) => {
            serde_json::to_value(did_doc).unwrap()
        },
        None => json!("No operations found for this DID")
    }
}

#[post("/<did>")]
fn post_did_operation(ops_store: &State<OperationsStore>, did: String) -> String {
    format!("Expects JSON encoded PLC operation as the body. This route will verify the operation is valid before adding it to the database of operations associated with {}.", did)
}

fn find_genesis_operation(operations: Vec<ValidatedSignedOperation>) -> Option<ValidatedSignedOperation> {
    for valid_op in operations {
        if valid_op.operation().r#type() == "create" {
            return Some(valid_op);
        }
    }
    None
}

fn check_did_plc(operations: String) -> Option<String> {
    None
}

async fn find_did_operations(
    ops_store: &OperationsStore,
    did: String,
    ) -> Option<Vec<ValidatedSignedOperation>> {
    let ops = ops_store.get_operations_for_did(&did)
                .await;

    let response = match ops {
        Ok(ops) => {
            ops
        },
        Ok(None) => None,
        Err(_) => unreachable!(),
    };
    response
}


/*
#[tokio::main]
async fn main() {
    // CONTEXT
    let signing_key = did_key::generate::<Secp256k1KeyPair>(None);
    let recovery_key = did_key::generate::<Secp256k1KeyPair>(None);
    let op = Operation::new()
        .r#type("create")
        .signing_key(format!("did:key:{}", &signing_key.fingerprint()))
        .recovery_key(format!("did:key:{}", &recovery_key.fingerprint()))
        .handle("jik.test")
        .service("https://example.pds")
        .build();
    let signed_operation = op.sign(&signing_key);
    let did = &signed_operation.did();
    let valid_signed_op = signed_operation.validate().unwrap();
    let ops_store = OperationsStore::new().await;
    let _op_cid = ops_store.insert(&valid_signed_op).await;
    let _ops = ops_store.get_operations_for_did(did).await;
    // END CONTEXT

    let did_doc_data = DidDocData::create(&valid_signed_op).unwrap();
    println!("{:#}", did_doc_data.generate_document());
}
*/


/*
async fn generate_did_operation(
    ops_store: &OperationsStore,
    did: String,
    ) -> Option<Vec<serde_json::Value>> {
    let ops = ops_store.get_operations_for_did(&did)
                .await;

    let response = match ops {
        Some(ops) => {
            ops
        },
        None => None
    };
    response
}
*/